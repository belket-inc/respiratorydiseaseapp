from error_codes import ErrorCodes


class DataAdministration:

    def __init__(self, data_manager_object):
        self.__data_manager = None
        self.__database = None
        self.__initialize(data_manager_object)

    def __initialize(self, data_manager_object):
        """
        This function starts initialization process
        :param data_manager_object: instance of DataManager class
        :return: -
        """
        self.__data_manager = data_manager_object
        self.set_model()

    def set_model(self, model=None):
        """
        This functions sets model, which will be used for predictions
        :param model: Model to be set
        :return: -
        """
        if model is None:
            self.__data_manager.set_working_model(self.__get_best_model())
        else:
            self.__data_manager.set_working_model(model)

    def fit_model(self, model, parameters=None):
        """
        This function fits model with new parameters
        :param model: model to fit
        :param parameters: parameters for fitting model
        :return: -
        """
        pass

    def __get_best_model(self):
        """
        This function gets model with best rate from database
        :return: model
        """
        pass

    def add_data_to_database(self):
        """
        This function adds new data into database
        :return: -
        """
        pass

    def remove_data_from_database(self):
        """
        This function removes data from database
        :return:
        """
        pass
