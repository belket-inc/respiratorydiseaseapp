from error_codes import ErrorCodes


class DataManager:

    def __init__(self):
        self.__working_model = None
        self.__initialize()

    def __initialize(self):
        """
        This function starts initialization process
        :return:
        """
        pass

    def set_working_model(self, model):
        """
        This function sets model as current working
        :param model: model to be set
        :return: -
        """
        self.__working_model = model

    def predict(self, data):
        """
        This function predicts result? using current working model
        :param data:
        :return:
        """
        def check_input_data(data):
            pass

        if check_input_data(data):
            result = self.__working_model.predict(data)
            return ErrorCodes.no_error, result
        else:
            return ErrorCodes.input_data_error, None
